import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("@/views/HomeView.vue"),
  },
  {
    path: "/about",
    name: "About",
    component: () => import("@/views/AboutView.vue"),
  },
  {
    path: "/option-api",
    name: "OptionAPI",
    component: () => import("@/components/OptionView.vue"),
  },
  {
    path: "/composition-api",
    name: "CompositionAPI",
    component: () => import("@/components/CompositionView.vue"),
  },
  {
    path: "/reactive-view",
    name: "ReactiveView",
    component: () => import("@/components/ReactiveView.vue"),
  },
  {
    path: "/two-way",
    name: "Twoway",
    component: () => import("@/components/TwoWay.vue"),
  },
  {
    path: "/:pathMatch(.*)*",
    name: "NotFound",
    component: () => import("@/views/NotFound.vue"),
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
